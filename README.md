# Coding interview questions for Data Scientists

Coding interviews are becoming more and more common for software development jobs and related fields, and even if you're not looking for a job right now, practicing for coding interviews is also a great way to become a better programmer in general.

Usually in a coding interview you're given a small problem to solve within 10 to 20 minutes. Most questions are designed to test your ability to write good code and think on your own. 

First of all, to show that you're a good programmer to the interviewer, you should be familiar with at least one major language, whether it's Python or R and SQL. 

Second of all, preparing for coding interviews is honing your problem-solving skills. To do this, you should practice solving as many problems as possible, and when you're solving these problems, I would write down your solution on paper first and then actually try running it to make sure it's correct. 

This repo is organized as follows: 

1. Python for algorithms and problem solving 
    - Lists 
    - 2D Arrays 
    - Dictionaries
    - Arrays  

2. Python for machine learning questions

3. R for statistical modeling questions 

4. SQL questions 

Additional resources: 

1. https://leetcode.com/
2. https://www.hackerrank.com/

